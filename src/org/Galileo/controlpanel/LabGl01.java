package org.Galileo.controlpanel;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.Galileo.info.panelinfo1;

/**
 *
 * @author Josue Daniel Roldan Ochoa 
 */

public class LabGl01 extends javax.swing.JFrame {
private int conteo = 0;    
public LabGl01() {
initComponents();
this.setLocationRelativeTo(null);
}

private void notas(){
tbl1.removeAll();
DefaultTableModel modelo= new DefaultTableModel();
modelo.addColumn("Notas");
String nm1= txnumero.getText();
int nm;
nm= Integer.parseInt(nm1);
Object[] a= new Object[nm];
for(int i=0; i<nm; i++){
a[i]= "";
modelo.addRow(a);
}
tbl1.setModel(modelo);
txrespuesta.setText("");
}
    
private void promedio(){
DefaultTableModel modelo= new DefaultTableModel();
int in = tbl1.getRowCount();
for(int i=0; i<in; i++){
int[] nd= new int[in];
String [] nd1= new String [in];
nd1[i]= (String) tbl1.getValueAt(i,0);
nd[i]= Integer.parseInt(nd1[i]);
conteo= nd[i]+conteo;
}
int c= conteo;
if( txrespuesta.getText().isEmpty()){  
txrespuesta.setText(""+c/in);
conteo=0;
}else{
}
}
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu2 = new javax.swing.JMenu();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl1 = new javax.swing.JTable();
        btaceptar = new javax.swing.JButton();
        txnumero = new javax.swing.JTextField();
        txrespuesta = new javax.swing.JTextField();
        btaceptar2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        tbl1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Notas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl1);

        btaceptar.setText("Aceptar");
        btaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btaceptarActionPerformed(evt);
            }
        });

        txnumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txnumeroKeyTyped(evt);
            }
        });

        txrespuesta.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btaceptar2.setText("Aceptar");
        btaceptar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btaceptar2ActionPerformed(evt);
            }
        });

        jMenu1.setText("Salir");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu3.setText("Info");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu3MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(txnumero)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txrespuesta)
                    .addComponent(btaceptar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btaceptar)
                    .addComponent(txnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txrespuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btaceptar2)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btaceptarActionPerformed
if(txnumero.getText().length()!=0){
notas();
}else{
JOptionPane.showMessageDialog(null,"INGRESE CANTIDAD DE NOTAS PARA GENERAR PROMEDIO!!!!");
}
    }//GEN-LAST:event_btaceptarActionPerformed

    private void btaceptar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btaceptar2ActionPerformed
promedio();      // TODO add your handling code here:
    }//GEN-LAST:event_btaceptar2ActionPerformed

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
System.exit(0);   // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1MousePressed

    private void txnumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnumeroKeyTyped
int k = (int) evt.getKeyChar();
if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
JOptionPane.ERROR_MESSAGE);
}   
int d = (int) evt.getKeyChar();
if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
JOptionPane.ERROR_MESSAGE);
}        // TODO add your handling code here:
    }//GEN-LAST:event_txnumeroKeyTyped

    private void jMenu3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MousePressed
      new panelinfo1().setVisible(true);  // TODO add your handling code here:
    }//GEN-LAST:event_jMenu3MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LabGl01.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LabGl01.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LabGl01.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LabGl01.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LabGl01().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btaceptar;
    private javax.swing.JButton btaceptar2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl1;
    private javax.swing.JTextField txnumero;
    private javax.swing.JTextField txrespuesta;
    // End of variables declaration//GEN-END:variables
}
