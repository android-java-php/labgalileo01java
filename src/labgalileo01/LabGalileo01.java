
package labgalileo01;

import org.Galileo.controlpanel.LabGl01;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class LabGalileo01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      new LabGl01().setVisible(true);
    }
    
}
